/**
 *
 * https://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html
 *
 */

import { Component, OnInit, Input, forwardRef } from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  Validator,
  ControlContainer,
  AbstractControl
} from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';
import { CustomValidators } from '../date.validator';

abstract class AbstractRegisterFunctions implements ControlValueAccessor {
  abstract writeValue(obj: any): void;

  protected propagateChange = (obj: any) => {};
  protected propagateTouched = () => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    // TODO
  }
}

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    }
  ]
})
export class DatepickerComponent extends AbstractRegisterFunctions implements OnInit, Validator {
  @Input() formControlName: string;

  datepicker: FormControl;
  control: AbstractControl;

  constructor(
    private controlContainer: ControlContainer
  ) { super(); }

  // Set initial Value
  writeValue(obj: any): void {
    this.datepicker.setValue(obj);
  }

  ngOnInit() {
    this.control = this.controlContainer.control.get(this.formControlName);

    this.datepicker = new FormControl('');
    this.datepicker.valueChanges
      .pipe(
        debounceTime(300),
        map(val => val.split('.').reverse().join('-'))
      )
      .subscribe(val => {
        this.propagateChange(val);
      });
  }

  validate(c: FormControl) {
    return CustomValidators.date()(c);
  }
}
