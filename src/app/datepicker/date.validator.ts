import { ValidatorFn, ValidationErrors } from '@angular/forms';
import * as moment from 'moment';

export class CustomValidators {
  static date(): ValidatorFn {
    return ({value}): ValidationErrors | null => {
      if (value === null || value === '') {
        return null;
      }

      if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) {
        return { format: {value} };
      }

      const date = moment(value, 'YYYY-MM-DD');
      if (!date.isValid()) {
        return { date: { value } };
      }
      return null;
    };
  }
}
