import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  form: FormGroup;
  val$: Observable<any>;

  constructor(
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      num: [5, [Validators.min(1), Validators.max(10)]],
      datepicker: ['11.12.1934', [Validators.required]],
    });

    this.val$ = this.form.valueChanges.pipe(
      debounceTime(300),
      map(val => JSON.stringify(val))
    );
  }
}
